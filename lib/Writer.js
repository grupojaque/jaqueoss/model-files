const fs = require('fs');
const util = require('util');
const writeFile = util.promisify(fs.writeFile);
const unlink = util.promisify(fs.unlink);

module.exports = {
  store: function(buffer, name) {
    return writeFile(name, buffer);
  },
  delete: function(name) {
    return unlink(name);
  }
};
