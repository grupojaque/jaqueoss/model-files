module.exports = class ModelFileError extends Error {

  constructor(status, message, errorDetail) {
    super(message);
    this.name = this.constructor.name;
    this.status = status;
    this.errorDetail = errorDetail;
    Error.captureStackTrace(this, this.constructor);
  }
};
