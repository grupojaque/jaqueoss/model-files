/**
* Configuration for model-files module
**/

module.exports = {
  /**
  * Objects with the options for each model atribute representing a file.
  */
  modelFiles: {
  },

  /**
  * Defined file types with options
  **/
  types: {
  },

  /**
  * Posible functions to run before store
  */

  tasks: {
  },


  /**
  * Gets the path for the given object
  **/
  getPath: function(gcsObject) {
  },

  /**
   * Gets the name of instance model
   * @param {*} instance 
   */
  getModelName: function(instance) {
  }


};
