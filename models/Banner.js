/**
* Using Sequelize
**/
module.exports = (sequelize, DataTypes) => {
  const Banner = sequelize.define('Banner', {
    has_mobile_picture: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    has_desktop_picture: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    has_gallery: {
      type: DataTypes.ARRAY(DataTypes.BOOLEAN),
      defaultValue: [false, false, false, false, false]
    }
  }, {
    underscored: true,
    timestamps: true,
    paranoid: true,
  });

  return Banner;
};
