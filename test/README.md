# Test

## Run tests

1. Create a postgres database to use in tests
2. Run tests using your postgres settings
    `POSTGRES_DATABASE=<Yourdatabase> POSTGRES_USER=<> POSTGRES_PASSWORD= npm test`
3. Add degub logs when runnign tests
    `DEBUG=<debugNameInFile> npm test`
4. Run lint
    `npm run lint`
