/**
* Database definition
*
* Defines the connection to the database and creates Sequelize
*/
/* global models */
const debug = require('debug')('db');
const Sequelize = require('sequelize');
const path = require('path');
const requireTree = require('require-tree');

const DataTypes = Sequelize.DataTypes;

const sequelize = new Sequelize(
  process.env.POSTGRES_DATABASE || 'test_db',
  process.env.POSTGRES_USER || 'root',
  process.env.POSTGRES_PASSWORD || '',
  {
    host: process.env.POSTGRES_HOST || 'localhost',
    dialect: 'postgres',
    operatorsAliases: false,
    logging: false,
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000,
    },
  }
);

sequelize
  .authenticate()
  .then(() => {
    debug('Connection has been established successfully.');
  })
  .catch(err => {
    debug('Unable to connect to the database:', err);
  });

module.exports = {
  sync() {
    const folder = path.join(process.cwd(), 'models');
    const definitions = requireTree(folder);
    debug(definitions);
    global.models = {};
    Object.keys(definitions).forEach(key => {
      const dbModel = definitions[key](sequelize, DataTypes);
      global[key] = dbModel;
      models[key] = dbModel;
    });
    debug(sequelize.models);
    Object.keys(sequelize.models).forEach(key => {
      debug(sequelize.models[key]);
      if(sequelize.models[key].associate) sequelize.models[key].associate(sequelize.models);
    });
    debug(models);
    return sequelize.sync({ force: true })
    .catch(err => {
      debug("Unable to sync sequelize");
      throw err;
    });
  },
  drop() {
    debug('drop data base');
    return sequelize.drop();
  },
  sequelize,
  Sequelize,
  DataTypes,
};
