const fs = require('fs');
const images = require('images');
const path = require('path');
const Pact = require('bluebird');;
const readFile = Pact.promisify(fs.readFile);
const should = require('should');
const testutils = require('./helpers/test_utils');

const config = require('../gcsFile.js');
const GCSFile = require('../index')(config);
const lodash = require('lodash');


unexpectedThen = function(){
  throw new Error("Unexpected promise resolved");
}

describe('GCSFile test', () => {
  describe('\n---------------------- Funtions ----------------------\n', () => {
    describe('\n- - - - - - - - - - -  create- - - - - - - - - - - \n', function (){
      it('should create a valid gcsFile', () =>{
        const localFilename = path.resolve(__dirname, '../test/helpers/images/testCorrect.png');
        let banner;
        return Banner.create()
        .then((bannerCreated) => {
          banner =  bannerCreated;
          return readFile(localFilename)
        })
        .then((fileRead) => {
          const gcsFile =  new GCSFile(banner,'mobile_picture',testutils.bufferToFileBusboy(fileRead,'image/png'),{brand: {slug:'something'}});
        })
      });
      it('should return error for model attribute not defined in config', () =>{
        let banner;
        return Banner.create()
        .then((bannerCreated) => {
          banner =  bannerCreated;
          const gcsFile =  new GCSFile(banner,'invalid_attribute');
        })
        .then(() => {
          true.should.be.false()
        })
        .catch((err)=> {
          err.message.should.startWith('Missing configuration in file config/gcsFile.js for attribute');
        })
      });
    });
    describe('\n- - - - - - - - - - -  getPath- - - - - - - - - - - \n', function (){
      it('should create a valid gcsFile get expected path and url using configurations', () =>{
        const localFilename = path.resolve(__dirname, '../test/helpers/images/testCorrect.png');
        let banner;
        return Banner.create()
        .then((bannerCreated) => {
          banner =  bannerCreated;
          return readFile(localFilename)
        })
        .then((fileRead) => {
          const gcsFile =  new GCSFile(banner,'mobile_picture',testutils.bufferToFileBusboy(fileRead,'image/png'),{brand: {slug:'something'}});
          const path =  gcsFile.getPath();
          path.should.eql('something-banners-'+banner.id+'-mobile_picture.png');
        })
      });
    });
    describe('\n- - - - - - - - - - -  updateInstance - - - - - - - - - - - \n', function (){
      it('should update instance attribute to true and false', () =>{
        const localFilename = path.resolve(__dirname, '../test/helpers/images/testCorrect.png');
        let banner, gcsFile;
        return Banner.create()
        .then((bannerCreated) => {
          banner =  bannerCreated;
          return readFile(localFilename)
        })
        .then((fileRead) => {
          gcsFile =  new GCSFile(banner,'mobile_picture',testutils.bufferToFileBusboy(fileRead,'image/png'),{brand: {slug:'something'}});
          return gcsFile.updateInstance();
        })
        .then((updatedInstance) => {
          updatedInstance.should.have.property('has_mobile_picture',true);
          return gcsFile.updateInstance(false);
        })
        .then((updatedInstance) => {
          updatedInstance.should.have.property('has_mobile_picture',false);
        })
      });
    });

    // This tests depend on the config/gcsFile.js
    describe('\n- - - - - - - - - - -  runBeforeStore - - - - - - - - - - - \n', function (){
      it('should validate a correct image', () =>{
        const localFilename = path.resolve(__dirname, '../test/helpers/images/testCorrect.png');
        let banner, gcsFile;
        return Banner.create()
        .then((bannerCreated) => {
          banner =  bannerCreated;
          return readFile(localFilename)
        })
        .then((fileRead) => {
          gcsFile =  new GCSFile(banner,'mobile_picture',testutils.bufferToFileBusboy(fileRead,'image/png'),{brand: {slug:'something'}});
          return gcsFile.runBeforeStore();
        })
      });
      it('should validate a correct image with other acepted extention', () =>{
        const localFilename = path.resolve(__dirname, '../test/helpers/images/testOtherExt.jpg');
        let banner, gcsFile;
        return Banner.create()
        .then((bannerCreated) => {
          banner =  bannerCreated;
          return readFile(localFilename)
        })
        .then((fileRead) => {
          gcsFile =  new GCSFile(banner,'mobile_picture',testutils.bufferToFileBusboy(fileRead,'image/jpg'),{brand: {slug:'something'}});
          return gcsFile.runBeforeStore();
        })
      });
      it('should validate a correct image with other acepted proportion', () =>{
        const localFilename = path.resolve(__dirname, '../test/helpers/images/testOtherSize.png');
        let banner, gcsFile;
        return Banner.create()
        .then((bannerCreated) => {
          banner =  bannerCreated;
          return readFile(localFilename)
        })
        .then((fileRead) => {
          gcsFile =  new GCSFile(banner,'mobile_picture',testutils.bufferToFileBusboy(fileRead,'image/png'),{brand: {slug:'something'}});
          return gcsFile.runBeforeStore();
        })
      });
      it('should return error for image with invalid extension', () =>{
        const localFilename = path.resolve(__dirname, '../test/helpers/images/testInvalidExt.gif');
        let banner, gcsFile;
        return Banner.create()
        .then((bannerCreated) => {
          banner =  bannerCreated;
          return readFile(localFilename)
        })
        .then((fileRead) => {
          gcsFile =  new GCSFile(banner,'mobile_picture',testutils.bufferToFileBusboy(fileRead,'image/gif'),{brand: {slug:'something'}});
          return gcsFile.runBeforeStore();
        })
        .then(unexpectedThen)
        .catch((err)=>{
          should.exist(err);
          err.should.have.property("name","ModelFileError");
          err.errorDetail[0].should.eql("unsupportedExtension");
          err.errorDetail[1].should.eql("testGenerated.png");
          err.should.have.property("message","Extension not supported");
        })
      });
      it('should return error for image with invalid proportion', () =>{
        const localFilename = path.resolve(__dirname, '../test/helpers/images/testInvalidProportion.png');
        let banner, gcsFile;
        return Banner.create()
        .then((bannerCreated) => {
          banner =  bannerCreated;
          return readFile(localFilename)
        })
        .then((fileRead) => {
          gcsFile =  new GCSFile(banner,'mobile_picture',testutils.bufferToFileBusboy(fileRead,'image/png'),{brand: {slug:'something'}});
          return gcsFile.runBeforeStore();
        })
        .then(unexpectedThen)
        .catch((err)=>{
          should.exist(err);
          err.should.have.property("name","ModelFileError");
          err.errorDetail[0].should.eql("unsupportedProportion");
          err.errorDetail[1].should.eql("testGenerated.png");
          err.should.have.property("message","Invalid proportion");
        })
      });
      it('should return error for image with other invalid size small', () =>{
        const localFilename = path.resolve(__dirname, '../test/helpers/images/testTooSmall.png');
        let banner, gcsFile;
        return Banner.create()
        .then((bannerCreated) => {
          banner =  bannerCreated;
          return readFile(localFilename)
        })
        .then((fileRead) => {
          gcsFile =  new GCSFile(banner,'mobile_picture',testutils.bufferToFileBusboy(fileRead,'image/png'),{brand: {slug:'something'}});
          return gcsFile.runBeforeStore();
        })
        .then(unexpectedThen)
        .catch((err)=>{
          should.exist(err);
          err.should.have.property("name","ModelFileError");
          err.errorDetail[0].should.eql("unsupportedSizeSmall");
          err.errorDetail[1].should.eql("testGenerated.png");
          err.should.have.property("message","Dimentions are too small");
        })
      });
      it('should return error for image too big', () =>{
        const localFilename = path.resolve(__dirname, '../test/helpers/images/testCorrect.png');
        let banner, gcsFile;
        return Banner.create()
        .then((bannerCreated) => {
          banner =  bannerCreated;
          return readFile(localFilename)
        })
        .then((fileRead) => {
          gcsFile =  new GCSFile(banner,'mobile_picture',testutils.bufferToFileBusboy(fileRead,'image/png',true)); //The change in trucat,{brand: testBrand}ed
          return gcsFile.runBeforeStore();
        })
        .then(unexpectedThen)
        .catch((err)=>{
          should.exist(err);
          err.should.have.property("name","ModelFileError");
          err.errorDetail[0].should.eql("unsupportedSizeBig");
          err.errorDetail[1].should.eql("testGenerated.png");
          err.should.have.property("message","Size is too big");
        })
      });
      it('should return error using a cofigured validation', () =>{
        const localFilename = path.resolve(__dirname, '../test/helpers/images/testCorrect.png');
        let banner, gcsFile;
        return Banner.create()
        .then((bannerCreated) => {
          banner =  bannerCreated;
          return readFile(localFilename)
        })
        .then((fileRead) => {
          configNew = lodash.cloneDeep(require('../gcsFile.js'));
          configNew.modelFiles.Banner.mobile_picture.beforeStore = ['validateWidthPair'];
          const GCSFile2 = require('../index')(configNew);
          gcsFile =  new GCSFile2(banner,'mobile_picture',testutils.bufferToFileBusboy(fileRead,'image/png'),{brand: {slug:'something'}});
          return gcsFile.runBeforeStore();
        })
        .then(unexpectedThen)
        .catch((err)=>{
          should.exist(err);
          err.should.have.property("name","ModelFileError");
          err.should.have.property("errorDetail","invalidWidth");
        })
      });
    });

    describe('\n- - - - - - - - - - -  store - - - - - - - - - - - \n', function (){
      it('should store and remove file', () =>{
        const localFilename = path.resolve(__dirname, '../test/helpers/images/testOtherSize.png');
        let banner, gcsFile;
        return Banner.create()
        .then((bannerCreated) => {
          banner =  bannerCreated;
          return readFile(localFilename)
        })
        .then((fileRead) => {
          gcsFile =  new GCSFile(banner,'mobile_picture',testutils.bufferToFileBusboy(fileRead,'image/png'),{brand: {slug:'something'}});
          return gcsFile.store();
        })
        .then(() => {
          const url = gcsFile.getFullUrl();
          url.should.eql('/files/something-banners-' + banner.id + '-mobile_picture.png');
          gcsFile.modelInstance.has_mobile_picture.should.be.true();
          return gcsFile.remove();
        })
        .then(() => {
          const url = gcsFile.getFullUrl();
          url.should.eql('');
        });
      });
      it('should store and image with diferent accepted extension', () =>{
        const localFilename = path.resolve(__dirname, '../test/helpers/images/testOtherExt.jpg');
        let banner, gcsFile;
        return Banner.create()
        .then((bannerCreated) => {
          banner =  bannerCreated;
          return readFile(localFilename)
        })
        .then((fileRead) => {
          gcsFile =  new GCSFile(banner,'mobile_picture',testutils.bufferToFileBusboy(fileRead,'image/jpg'),{brand: {slug:'something'}});
          return gcsFile.store();
        })
        .then(() => {
          return gcsFile.getFullUrl();
        })
        .then((url) => {
          url.should.eql('/files/something-banners-' + banner.id + '-mobile_picture.png');
          gcsFile.modelInstance.has_mobile_picture.should.be.true();
        })
      });

    });



  });


});
