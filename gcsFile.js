/**
* Configuration for files managed by the module
**/
const ModelFileError = require('./lib/ModelFileError');

module.exports = {
  modelFiles: {
    Banner: {
      desktop_picture: {
        type: 'pngImage',
        options: {
          width: 952,
          height: 596
        },
      },
      mobile_picture: {
        type: 'pngImage',
        options: {
          width: 385,
          height: 241
        },
      },
      gallery: {
        type: 'pngImage',
        options: {
          width: 790,
          height: 500
        },
      },
      privacy_notice: {
        type: 'pdf',
      },
    },
  },

  types: {
    pngImage: {
      ext: '.png',
      acceptedMimes: [
        'image/jpeg',
        'image/jpg',
        'image/png'
      ],
      mimeType: 'image/png',
      encodingFormat: 'png',
      encodingCompresionRate: 85,
      beforeStore: ['validateNotBiggerSize', 'validateHasStoredData', 'validateExtension', 'validateNotSmallerDimentions', 'validateSameProportion', 'encode']
    },
    pdf: {
      ext: '.pdf',
      acceptedMimes: ['application/pdf'],
      mimeType: 'application/pdf',
      beforeStore: ['validateNotBiggerSize', 'validateHasStoredData', 'validateExtension']
    }
  },

  tasks: {
    validateWidthPair: function() {
      throw new ModelFileError('invalidWidth', 'InvalidWidth', 'invalidWidth');
    }
  },

  filesDir: 'assets',

  maxSize: '1mb', //Hardcoded directly in config/http.js

  getPath: function(gcsObject) {
    const slug = gcsObject.extraOptions.brand.slug;
    const modelNamePlural = this.getModelNamePlural(gcsObject.modelInstance);
    const modelId = gcsObject.modelInstance.id;
    const fileName = gcsObject.fileName;
    const extension = gcsObject.type.ext;

    return slug + '-' + modelNamePlural + '-' + modelId + '-' + fileName + extension;
  },

  getModelNameSingular: function(instance) {
    return instance._modelOptions.name.singular;
  },
  getModelNamePlural: function(instance) {
    return instance._modelOptions.name.plural.toLowerCase();
  }
};
