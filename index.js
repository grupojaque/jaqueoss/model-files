/** DEFAULT Config **/
const ModelFileError = require('./lib/ModelFileError');
const writer = require('./lib/Writer');
const images = require('images');
const lodash = require('lodash');
const config = {
  modelFiles: {},
  types: {},
  filesDir: 'assets',
  prefixURL: 'files',
  getPath: function() {
    return '';
  },

  tasks: {
    validateExactDimentions: function(gcsFile) {
      const image = images(gcsFile.inputFile.data);
      const invalidWidth = gcsFile.options.width!=image.width();
      const invalidHeight = gcsFile.options.height!=image.height();

      if (invalidWidth || invalidHeight) {
        throw new ModelFileError('unsupportedMediaType', 'Dimensions must be exact', ['unsupportedSize', gcsFile.inputFile.name, gcsFile.options.height, gcsFile.options.width]);
      }
    },
    validateNotBiggerSize: function(gcsFile) {
      const hasOverpasedMaxSize = gcsFile.inputFile.truncated;

      //TODO add Max size option to each model
      if (hasOverpasedMaxSize) {
        throw new ModelFileError('unsupportedMediaType', 'Size is too big', ['unsupportedSizeBig', gcsFile.inputFile.name, '1mb']);
      }
    },
    validateHasStoredData: function(gcsFile) {
      const hasData = gcsFile.inputFile.data != null;

      if (!hasData) {
        throw new ModelFileError('unsupportedMediaType', 'Media type not supported', 'unsupportedMediaType');
      }
    },
    validateExtension: function(gcsFile) {
      const hasValidExtension = gcsFile.type.acceptedMimes.includes(gcsFile.inputFile.mimetype);

      if (!hasValidExtension) {
        const acceptedMimes= gcsFile.type.acceptedMimes.map((m) => {return m.split('/')[1];});

        throw new ModelFileError('unsupportedMediaType', 'Extension not supported', ['unsupportedExtension', gcsFile.inputFile.name, acceptedMimes]);
      }
    },
    validateNotSmallerDimentions: function(gcsFile) {
      const image = images(gcsFile.inputFile.data);
      const hasSmallerWidth = image.width() < gcsFile.options.width;
      const hasSmallerHeight = image.height() < gcsFile.options.height;

      if (hasSmallerWidth || hasSmallerHeight) {
        throw new ModelFileError('unsupportedMediaType', 'Dimentions are too small', ['unsupportedSizeSmall', gcsFile.inputFile.name, gcsFile.options.height, gcsFile.options.width]);
      }
    },
    validateSameProportion: function(gcsFile) {
      const image = images(gcsFile.inputFile.data);
      const proportion = image.width() / image.height();
      const expectedProportion = gcsFile.options.width / gcsFile.options.height;

      if (proportion != expectedProportion) {
        throw new ModelFileError('unsupportedMediaType', 'Invalid proportion', ['unsupportedProportion', gcsFile.inputFile.name, gcsFile.options.height, gcsFile.options.width], 'Invalid proportion');
      }
    },
    encode: function(gcsFile) {
      const hasExpectedExtension = gcsFile.type.mimeType == gcsFile.inputFile.mimetype;

      if (hasExpectedExtension) {
        return;
      }
      const image = images(gcsFile.inputFile.data);

      gcsFile.inputFile.data = image.encode(gcsFile.type.encodingFormat, {operation: gcsFile.type.encodingCompresionRate});
    },
    getModelNameSingular: function(instance) {
      return instance._modelOptions.name.singular;
    }
  }
};


/** CLASS **/
module.exports = function(configCustom) {
  configCustom = lodash.defaultsDeep(configCustom, config);

  return class GCSFile {

    /**
    *modelInstance: model asociated to image. Must have attribute called 'has_' + fileName
    *               this must be a boolean or an array of booleans in case of a gallery
    *fileName: name for the file. Without extension.
    *inputFile: the input file generated with busboy-body-parser in config/http.js
    *object: Options to create path and others
    **/
    constructor(modelInstance, fileName, inputFile, extraOptions) {
      const modelName = configCustom.getModelNameSingular(modelInstance);
      const modelConfig = configCustom.modelFiles[modelName];

      if (!modelConfig) {
        throw new ModelFileError('serverError', 'Missing configuration in file config/gcsFile.js for model ' + modelName, 'serverError');
      }
      this.modelInstance = modelInstance;
      this.fileName = fileName;
      const nameSplit = this.fileName.split('-');
      const fileConfig = modelConfig[nameSplit[0]];

      if (!fileConfig) {
        throw new ModelFileError('serverError', 'Missing configuration in file config/gcsFile.js for attribute ' + nameSplit[0], 'serverError');
      }
      const typeName = fileConfig.type;

      this.type = configCustom.types[typeName];
      this.options = fileConfig.options;
      const beforeStore = this.type.beforeStore || [];

      this.beforeStore = beforeStore.concat(fileConfig.beforeStore||[]);
      this.inputFile = inputFile;
      this.extraOptions = extraOptions;
    }

    modelHasFile() {
      const nameSplit = this.fileName.split('-');
      let booleanOrArray = this.modelInstance['has_'+nameSplit[0]];

      if (nameSplit.length>1) {
        const pos = parseInt(nameSplit[1]) - 1;

        booleanOrArray = booleanOrArray[pos];
      }
      return booleanOrArray == true;
    }

    //Gets path for file
    getPath() {
      return configCustom.getPath(this);
    }

    //Constructs the url.
    //For this url to work correctly is important to set the cache as private when storing
    getFullUrl() {
      if (!this.modelHasFile()) {
        return '';
      }
      const filePath = this.getPath();

      return '/' + configCustom.prefixURL + '/' + filePath;
    }

    runBeforeStore() {
      return new Promise((resolve) => {
        this.beforeStore.forEach((procedureName) => {
          const validationFunction = configCustom.tasks[procedureName];

          if (!validationFunction) {
            throw new ModelFileError('serverError', 'Validation ' + procedureName + ' not configured', 'serverError');
          }
          validationFunction(this);
        });

        resolve(null);
      });
    }

    //Updates the model instance attribute called 'has_' + fileName
    updateInstance(hasFile = true) {
      const updateData = {};
      const nameSplit = this.fileName.split('-');

      if (nameSplit.length>1) {
        const pos = parseInt(nameSplit[1]) - 1;
        const oldArray = this.modelInstance['has_'+nameSplit[0]];

        oldArray[pos] = hasFile;
        updateData['has_'+nameSplit[0]] = oldArray;
      } else {
        updateData['has_'+nameSplit[0]] = hasFile;
      }
      return this.modelInstance.update(updateData)
        .then((updatedInstance) => {
          this.modelInstance = updatedInstance;
          return updatedInstance;
        });
    }

    //Stores the file. Validates, encodes, stores and updates model.
    store() {
      return this.runBeforeStore()
        .then(() => {

          const filePath = this.getPath();
          const options = {metadata: {}};

          options.metadata.mimeType = this.type.mimeType;

          return writer.store(this.inputFile.data, configCustom.filesDir + '/' + filePath, options);
        })
        .then(() => {
          return this.updateInstance();
        })
        .catch((err) => {
          throw err;
        });
    }

    //Deletes file. Delete from server and update model.
    remove() {
      return new Promise((resolve) => {
        const filePath = this.getPath();

        if (!this.modelHasFile()) {
          return resolve();
        }


        return resolve(writer.delete(configCustom.filesDir + '/' +filePath));
      })
        .then(() => {
          return this.updateInstance(false);
        })
        .catch((err) => {
          throw err;
        });
    }
  };

};
